/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, SafeAreaView, ScrollView
} from 'react-native';
import Header from './src/components/header';
import axios from 'axios'
import AlbumList from './src/components/albumList';

export default class App extends Component {
    constructor(){
      super()
      this.state = {
        data:[]
      }
    }

    render(){
      return(
        <View style={{flex:1 }}>
          <Header headerText = 'Albums !'/>
          <AlbumList/>
        </View>
      )
    }
}