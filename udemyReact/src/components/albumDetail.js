import React from 'react';
import {View, Text, StyleSheet, Image, Alert, TouchableOpacity, Linking} from 'react-native';
import Card from './card';
import Button from './Button';
import CardSection from './cardSection'

export default  class AlbumDetail extends React.Component{
    render(){

        return(
            <Card>
                <CardSection>
                    <View style={styles.thubnailContainer}>
                        <Image
                        style={{width: 50, height:50}}
                            source={{uri:this.props.album.thumbnail_image}}
                        />
                    </View>

                    <View style={styles.containerText}>
                        <Text style={styles.headerTextStyle}>{this.props.album.title}</Text>
                        <Text>{this.props.album.artist}</Text>    
                    </View>                                      
                
                </CardSection>

                <CardSection>
                    <Image
                    style={styles.imageContainer}
                        source={{uri: this.props.album.image}}
                    />
                </CardSection>

                {/* <CardSection> */}
                    <TouchableOpacity onPress={ ()=>Linking.openURL(this.props.album.url) }>
                        <Button >Buy Now</Button>
                    </TouchableOpacity>        
                {/* </CardSection> */}
            </Card>
        )
    }
} 

const styles = StyleSheet.create({
    containerText:{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'flex-start',
        marginLeft: 10
    },

    thubnailContainer:{
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
        marginRight: 5
    },
    headerTextStyle:{
        fontSize:18,
        color:'black',
        
    },
    imageContainer:{
        flex:1,
        width: null,
        height:350,
        justifyContent:'center',
        alignItems: 'center',
        // marginLeft: 20,
    }
})