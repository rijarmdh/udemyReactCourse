import React from 'react';
import { View, Text, StyleSheet, ScrollView} from 'react-native';
import axios from 'axios'
import AlbumDetail from './albumDetail';
export default class AlbumList extends React.Component{
    constructor(){
        super()
        this.state= {
            albums:[]
        }
    }

    componentWillMount(){
        axios.get("http://rallycoding.herokuapp.com/api/music_albums")
            .then(
                (res)=>{
                    console.log(res.data)
                    albumData = []
                    res.data.map(
                        (data)=>albumData.push(data)
                    )
                    this.setState({albums: albumData})
                }
            )
            .catch(
                (err)=>console.log(err)
            )
    }

    getData(){
       return this.state.albums.map(
            (data)=><AlbumDetail key={data.title} album={data}/>
        )
    }

    render(){
        console.log('state data : ', this.state.albums) 

        return(
            <ScrollView>
                {this.getData()}
            </ScrollView>
            )
    }
}
