import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default class Header extends React.Component{
    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.text}>{this.props.headerText}</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-start',
        backgroundColor: '#f8f8f8',
        paddingTop: 20,
        height:60,
        shadowColor: '#000',
        shadowOpacity: 0.5,
        shadowOffset: {width:0, height:20},
        elevation: 2,
        position: 'relative'
    },
    text:{
        fontSize: 20,
        fontWeight: 'normal',
    }
})