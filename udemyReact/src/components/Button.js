import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Button = (props) => {
    const {buttonStyle, textStyle} = styles
    return (
        <View style={buttonStyle}>
           <Text style={textStyle}> {props.children} </Text>
        </View>
    )
}

const styles= StyleSheet.create({
    buttonStyle:{
        flex: 1, 
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#007aff',
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 5,
        marginTop: 5
    },
    textStyle:{
        alignSelf: 'center',
        color: '#007aff',
        fontSize: 16,
        fontWeight: "600",
        paddingTop: 10,
        paddingBottom: 10,
    }
})
export default Button;